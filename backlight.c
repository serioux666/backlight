/*
 * backlight
 * Copyright (C) 2023  Gregor Vollmer (git@dynamic-noise.net)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <assert.h>

#include "config.h"

#define ARRAY_SIZE(X) (sizeof(X) / sizeof(*X))
#define UNUSED(X) (void)(X);

struct command {
  const char *cmd;
  int nargs;
  void (*callback)(int*,int,char **);
};

static void cmd_get(int *brightness, int argc, char **argv);
static void cmd_set(int *brightness, int argc, char **argv);
static void cmd_inc(int *brightness, int argc, char **argv);
static void cmd_dec(int *brightness, int argc, char **argv);

static const struct command COMMANDS[] = {
  { "-get", 0, &cmd_get },
  { "-set", 1, &cmd_set },
  { "-inc", 1, &cmd_inc },
  { "-dec", 1, &cmd_dec },
};

char *argv0 = NULL;

static int get_maximum_brightness(void)
{
  FILE *file = fopen(DEV_MAX_BRIGHTNESS, "r");
  int val = 0;
  if(file) {
    fscanf(file, "%i", &val);
    fclose(file);
  } else {
    perror("Cannot open max_brightness device file");
    exit(1);
  }
  return val;
}

static int get_brightness(void)
{
  FILE *file = fopen(DEV_BRIGHTNESS, "r");
  int val = 0;
  if(file) {
    fscanf(file, "%i", &val);
    fclose(file);
  } else {
    perror("Cannot open brightness device file");
  }
  return val;
}

static void set_brightness(int val)
{
  FILE *file = fopen(DEV_BRIGHTNESS, "w");
  if(file) {
    fprintf(file, "%i", val);
    fclose(file);
  } else {
    perror("Cannot open brightness device file");
  }
}

static void print_usage(void)
{
  printf("Usage: %s [-inc N] [-dec N] [-set N] [-get]\n", argv0);
}

static void parse_args(int *brightness, int argc, char *argv[])
{
  while(argc && argv && argv[0]) {
    bool found = false;
    for(int i = 0; i < ARRAY_SIZE(COMMANDS) && !found; ++i) {
      assert(COMMANDS[i].callback);
      assert(COMMANDS[i].cmd);
      if(strcmp(COMMANDS[i].cmd, argv[0]) == 0) {
        found = true;
        if(argc + 1 < COMMANDS[i].nargs) {
          fprintf(stderr, "%s: Error: Not enough arguments left for command '%s'\n", argv0, argv[0]);
          print_usage();
          return;
        }
        COMMANDS[i].callback(brightness, argc - 1, argv + 1);
        if(*brightness > 100) {
          *brightness = 100;
        }
        if(*brightness < MINIMUM_BRIGHTNESS_PERCENT) {
          *brightness = MINIMUM_BRIGHTNESS_PERCENT;
        }
        argc -= COMMANDS[i].nargs + 1;
        argv += COMMANDS[i].nargs + 1;
      }
    }
    if(!found) {
      fprintf(stderr, "%s: Error: Unparsed command line argument '%s'\n", argv0, argv[0]);
      print_usage();
      return;
    }
  }
}

void cmd_get(int *brightness, int argc, char **argv)
{
  UNUSED(argc);
  UNUSED(argv);
  printf("%i\n", *brightness);
}

void cmd_set(int *brightness, int argc, char **argv)
{
  UNUSED(argc);
  *brightness = atoi(*argv);
}

void cmd_inc(int *brightness, int argc, char **argv)
{
  UNUSED(argc);
  *brightness += atoi(*argv);
}

void cmd_dec(int *brightness, int argc, char **argv)
{
  UNUSED(argc);
  *brightness -= atoi(*argv);
}

static int get_brightness_percent(void)
{
  float x = (float)get_brightness() / get_maximum_brightness();
  return lroundf(powf(x, 1.0f / GAMMA) * 100.0f);
}

static void set_brightness_percent(int percent)
{
  float x = percent / 100.0f;
  int brightness = lroundf(powf(x, GAMMA) * get_maximum_brightness());
  if(percent > 0 && brightness == 0) {
    brightness = 1;
  }
  set_brightness(brightness);
}

int main(int argc, char *argv[])
{
  argv0 = argv[0];
  int brightness = get_brightness_percent();
  if(argc == 1) {
    print_usage();
  } else {
    int initial = brightness;
    parse_args(&brightness, argc - 1, argv + 1);
    if(brightness != initial) {
      set_brightness_percent(brightness);
    }
  }
  return 0;
}
