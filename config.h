/*
 * backlight
 * Copyright (C) 2023  Gregor Vollmer (git@dynamic-noise.net)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CONFIG_H
#define CONFIG_H

/* System device file returning the maximum brightness value allowed */
#define DEV_MAX_BRIGHTNESS          "/sys/class/backlight/intel_backlight/max_brightness"

/* System device for getting/setting the backlight brightness */
#define DEV_BRIGHTNESS              "/sys/class/backlight/intel_backlight/brightness"

/* Minimum value for the [0..100] brightness value used in the utility.
 * Avoid switching off the backlight!
 */
#define MINIMUM_BRIGHTNESS_PERCENT  10

/* Exponent of gamma curve */
#define GAMMA                       2.2f


#endif /* CONFIG_H */
