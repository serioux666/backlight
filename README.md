# backlight - Simple backlight controls

I needed a small utility to modify my backlight levels, none of the usual candidates seemed to
work properly. Yet, writing to the sysfs files for the backlight class worked like a charm...

## Usage

   Usage: backlight [-inc N] [-dec N] [-set N] [-get]

- `-inc N`: Increase brightness by value `N`
- `-dec N`: Decrease brightness by value `N`
- `-set N`: Set brightness to value `N`
- `-get`: Get current brightness value

The brightness is a value between 0 and 100, which is applied to the backlight with
a gamma correction. The minimum value is actually clamped, as can be configured.


## Configuration

Following the conventions from the great *suckless* utilities, configuration can be done
by modifying the source code :-) Adjust the values in `config.h` to your liking.

## Building and Installing

    $ make
    $ sudo make install

The `install` target will by default set the sticky bit on the executable, so it is not
required to call the utility with sudo. If you have safety concerns, you can add
special sudo rules for users on your machine.

## License
Copyright (C) 2023  Gregor Vollmer (git@dynamic-noise.net)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

