CC=gcc
LD=gcc
CFLAGS=-g
LDFLAGS=-lm
LIBS=-lm
INSTALL_PREFIX=/usr
INSTALL_OWNER=root:root
INSTALL_PERMISSIONS=a+rwxs

obj = backlight.o

.PHONY: all
all: backlight

.PHONY: clean
clean:
	rm ${obj} backlight

%.o: %.c
	${CC} ${CFLAGS} -c -o $@ $<

backlight: ${obj}
	${LD} ${LDFLAGS} -o backlight $< ${LIBS}

.PHONY: install
install:
	cp backlight ${INSTALL_PREFIX}/bin
	chown ${INSTALL_OWNER} ${INSTALL_PREFIX}/bin/backlight
	chmod ${INSTALL_PERMISSIONS} ${INSTALL_PREFIX}/bin/backlight
